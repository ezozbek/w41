import React, { useState } from 'react';

const Interval = () => {
  const [intervalTimes, setIntervalTimes] = useState([]);
  const [currentTime, setCurrentTime] = useState(null);

  const recordInterval = () => {
    const timestamp = Date.now();
    setIntervalTimes(prevTimes => [...prevTimes, timestamp]);
    setCurrentTime(timestamp);
  };

  return (
    <div>
      <h1>Interval Clock</h1>
      <button onClick={recordInterval} style={{ fontSize: 36 }}>
        Interval Time
      </button>
      <table>
        <thead>
          <tr>
            <th>Interval Time</th>
          </tr>
        </thead>
        <tbody>
          {intervalTimes.map((time, index) => (
            <tr key={index}>
              <td>{formatTime(time)}</td>
            </tr>
          ))}
        </tbody>
      </table>
      {currentTime && (
        <div>
          <h2>Current Time</h2>
          <p>{formatTime(currentTime)}</p>
        </div>
      )}
    </div>
  );
};

const formatTime = (timestamp) => {
  const date = new Date(timestamp);
  const hours = String(date.getHours()).padStart(2, '0');
  const minutes = String(date.getMinutes()).padStart(2, '0');
  const seconds = String(date.getSeconds()).padStart(2, '0');
  const milliseconds = String(date.getMilliseconds()).padStart(3, '0');

  return `${hours}:${minutes}:${seconds}.${milliseconds}`;
};

export default Interval;
