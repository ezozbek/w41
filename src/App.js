import React from 'react';
import Interval from './Interval.js'; 

const App = () => {
  return (
    <div>
      <h1>Interval Clock</h1>
      <Interval />
    </div>
  );
};

export default App;
